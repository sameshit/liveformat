#ifndef LIVETEST__HHH
#define LIVETEST__HHH

#include "../src/LiveReader.h"
#include "../src/LiveWriter.h"

namespace LiveFormatLib
{
	class LiveTest
	{
	public:
		LiveTest(CoreObjectLib::CoreObject* core);
		virtual ~LiveTest();

		bool Run();
	private:
		CoreObjectLib::CoreObject *_core;
		LiveReader *_reader;
		LiveWriter *_writer;
		uint64_t _event_id;

		void ProcessVideoStream(const uint16_t &stream_id,const VideoHeader &header);
		void ProcessAudioStream(const uint16_t &stream_id,const AudioHeader &header);
		void ProcessPacket(const uint16_t &stream_id,const Packet &pkt);
		void ProcessCustomStream(const uint16_t &stream_id,const std::string &header);
		void ProcessChatStream(const uint16_t &stream_id);
		void ProcessCommandStream(const uint16_t &stream_id);
		void ProcessSubtitleStream(const uint16_t &stream_id);
		void ProcessCloseStream(const uint16_t &stream_id);
		

		void ProcessChunk(const std::string &chunk);
	};
}

#endif