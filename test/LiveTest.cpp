#include "LiveTest.h"

using namespace CoreObjectLib;
using namespace std;
using namespace LiveFormatLib;

LiveTest::LiveTest(CoreObject *core)
	:_core(core)
{
	fast_new(_reader);
	fast_new(_writer);

	_reader->OnOpenAudioStream.Attach(this,&LiveTest::ProcessAudioStream);
	_reader->OnOpenVideoStream.Attach(this,&LiveTest::ProcessVideoStream);
	_reader->OnPacket.Attach(this,&LiveTest::ProcessPacket);
	_reader->OnOpenChatStream.Attach(this,&LiveTest::ProcessChatStream);
	_reader->OnOpenCommandStream.Attach(this,&LiveTest::ProcessCommandStream);
	_reader->OnOpenSubtitleStream.Attach(this,&LiveTest::ProcessSubtitleStream);
	_reader->OnOpenCustomStream.Attach(this,&LiveTest::ProcessCustomStream);
	_reader->OnCloseStream.Attach(this,&LiveTest::ProcessCloseStream);

	_event_id = _writer->OnChunk.Attach(this,&LiveTest::ProcessChunk);
}

LiveTest::~LiveTest()
{
	fast_delete(_reader);
	fast_delete(_writer);
}

bool LiveTest::Run()
{
	AudioHeader audio_header;
	VideoHeader video_header;
	Packet pkt;
	uint16_t video_stream,audio_stream,subtitle_stream,chat_stream,command_stream,custom_stream;

	audio_header.bitstream_size = 1;
	audio_header.bit_rate = 20;
	audio_header.channels = 5;
	audio_header.codec = LiveCodec::AAC;
	audio_header.extra_data = "hello";
	audio_header.max_bit_rate = 2;
	audio_header.sample_rate = 4;

	LOG_INFO("AudioHeader. BitstreamSize: "<<audio_header.bitstream_size<<", bitrate: "<<audio_header.bit_rate
		<<", channels: "<<audio_header.channels<<", codec: "<<(int)audio_header.codec<<", extra: "
		<<audio_header.extra_data<<", max_bit_rate: "<<audio_header.max_bit_rate<<", sample rate: "<<
		audio_header.sample_rate);

	video_header.bit_rate = 1;
	video_header.codec = LiveCodec::H264;
	video_header.extra_data = "www";
	video_header.height = 100;
	video_header.width = 200;

	LOG_INFO("VideoHeader. BitRate: "<<video_header.bit_rate<<", codec: "<<(int)video_header.codec
		<<", extra: "<<video_header.extra_data<<", height: "<<video_header.height<<
		", width: "<<video_header.width);

	audio_stream = _writer->OpenAudioStream(audio_header);
	video_stream = _writer->OpenVideoStream(video_header);

	pkt.data.resize(500);
	for (int i = 0; i < 30; ++i)
	{
		pkt.pts = i;
		if (i%2==0){
			RETURN_IF_FALSE(_writer->WritePacket(audio_stream,pkt));}
		else{
			RETURN_IF_FALSE(_writer->WritePacket(video_stream,pkt));}
	}
	_writer->Flush();

	RETURN_MSG_IF_FALSE(_writer->SerializeStreams() == _reader->SerializeStreams(),"Serialized streams are not equal");

	RETURN_IF_FALSE(_writer->CloseStream(audio_stream));
	RETURN_IF_FALSE(_writer->CloseStream(video_stream));

	subtitle_stream = _writer->OpenSubtitleStream();
	chat_stream = _writer->OpenChatStream();
	command_stream = _writer->OpenCommandStream();
	custom_stream = _writer->OpenCustomStream(string("hello world"));

	RETURN_IF_FALSE(_writer->WritePacket(subtitle_stream,pkt));
	RETURN_IF_FALSE(_writer->WritePacket(chat_stream,pkt));
	RETURN_IF_FALSE(_writer->WritePacket(command_stream,pkt));
	RETURN_IF_FALSE(_writer->WritePacket(custom_stream,pkt));

	_writer->Flush();

	RETURN_MSG_IF_FALSE(_writer->SerializeStreams() == _reader->SerializeStreams(),"Serialized streams are not equal 2");

	pkt.data.resize(kLiveChunkSize/2);
	pkt.pts = 0;
	_writer->OnChunk.Deattach(_event_id);
	RETURN_IF_FALSE(_writer->WritePacket(subtitle_stream,pkt));
	RETURN_IF_FALSE(_writer->WritePacket(command_stream,pkt));
	_writer->OnChunk.Attach(this,&LiveTest::ProcessChunk);
	_reader->ReOpen();
	RETURN_IF_FALSE(_reader->Read(_writer->SerializeStreams()));
	RETURN_IF_FALSE(_writer->WritePacket(chat_stream,pkt));
	_writer->Flush();

	pkt.data.resize(3908); // big test
	RETURN_IF_FALSE(_writer->WritePacket(chat_stream,pkt));
	_writer->Flush();

	pkt.data.resize(1284); // size test
	RETURN_IF_FALSE(_writer->WritePacket(command_stream,pkt));
	RETURN_IF_FALSE(_writer->WritePacket(command_stream,pkt));
	RETURN_IF_FALSE(_writer->WritePacket(command_stream,pkt));
	_writer->Flush();

	return true;
}

void LiveTest::ProcessChunk(const string &chunk)
{
//	LOG_INFO("Processing chunk");
	if (!_reader->Read(chunk))
		LOG_ERROR("Read error: "<<COErr::Get());
}

void LiveTest::ProcessAudioStream(const uint16_t &stream_id,const AudioHeader &audio_header)
{
	LOG_INFO("AudioHeader. Stream id:"<<stream_id<<" bitstreamSize: "<<audio_header.bitstream_size
		<<", bitrate: "<<audio_header.bit_rate
		<<", channels: "<<audio_header.channels<<", codec: "<<(int)audio_header.codec<<", extra: "
		<<audio_header.extra_data<<", max_bit_rate: "<<audio_header.max_bit_rate<<", sample rate: "<<
		audio_header.sample_rate);
}

void LiveTest::ProcessVideoStream(const uint16_t &stream_id,const VideoHeader &video_header)
{
	LOG_INFO("VideoHeader. Stream id:"<<stream_id<<" bitRate: "<<video_header.bit_rate<<", codec: "<<(int)video_header.codec
		<<", extra: "<<video_header.extra_data<<", height: "<<video_header.height<<
		", width: "<<video_header.width);
}

void LiveTest::ProcessPacket(const uint16_t &stream_id,const Packet &pkt)
{
	LOG_INFO("Stream: "<<stream_id<<", received pkt #: "<<pkt.pts);
}

void LiveTest::ProcessChatStream(const uint16_t &stream_id)
{
	LOG_INFO("Chat stream: "<<stream_id);
}

void LiveTest::ProcessCommandStream(const uint16_t &stream_id)
{
	LOG_INFO("Command stream: "<<stream_id);
}

void LiveTest::ProcessSubtitleStream(const uint16_t &stream_id)
{
	LOG_INFO("Subtitle stream: "<<stream_id);
}

void LiveTest::ProcessCustomStream(const uint16_t &stream_id,const std::string &header)
{
	LOG_INFO("Custom stream: "<<stream_id<<", header size: "<<header.size()<<", header: "<<header);
}

void LiveTest::ProcessCloseStream(const uint16_t &stream_id)
{
	LOG_INFO("Stream "<<stream_id<<"closed");
}