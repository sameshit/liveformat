#include "LiveTest.h"

using namespace LiveFormatLib;
using namespace CoreObjectLib;
using namespace std;

int main()
{
	CoreObject *_core;
	LiveTest *test;

	_core = new CoreObject;
	fast_new(test,_core);
	ABORT_IF_FALSE(test->Run());
	cin.get();
	fast_delete(test);
	delete _core;
	return 0;
}