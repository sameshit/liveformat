//
//  LiveReader.h
//  Decoder
//
//  Created by Oleg on 07.02.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#ifndef __Decoder__LiveReader__
#define __Decoder__LiveReader__

#include "LiveBase.h"


namespace LiveFormatLib
{   
	enum class LiveReaderState
	{
		Idle,
		Size,
		Part,
	};

    class LiveReader
		:public LiveBase
    {
    public:
        LiveReader();
        virtual ~LiveReader();
        
        void ReOpen();
        bool Read(const std::string &chunk);
		
		LiveAudioStreamEvent	OnOpenAudioStream;
		LiveVideoStreamEvent	OnOpenVideoStream;
		LiveChatStreamEvent		OnOpenChatStream;
		LiveCommandStreamEvent  OnOpenCommandStream;
		LiveSubtitleStreamEvent OnOpenSubtitleStream;
		LiveCustomStreamEvent   OnOpenCustomStream;
		LiveCloseStreamEvent	OnCloseStream;
		LivePacketEvent			OnPacket;
	private:
		uint32_t _last_chunk_id;
		std::string _part;
		bool _next_is_part;
		LivePacketType _pkt_type;
		uint32_t	   _pkt_size,_parsed_pkt_size;
		uint8_t		   _packed_pkt_size[4];
		LiveReaderState	_state;
		uint8_t *_pos,*_end;

		bool ParsePacketToEnd();
		bool ParseChunk(const std::string &chunk);
		bool ParsePackedData(const LivePacketType &type);
		bool ParseStreams(const std::string &chunk);
		bool ParseOpenStream(uint8_t *pos,uint8_t *end);
		bool ParseCloseStream(uint8_t *pos,uint8_t *end);
		bool ParsePacket(uint8_t *pos,uint8_t *end);
    };
}

#endif /* defined(__Decoder__LiveReader__) */
