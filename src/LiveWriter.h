#ifndef LIVEWRITER__H
#define LIVEWRITER__H

#include "LiveBase.h"

namespace LiveFormatLib
{
	class LiveWriter
		:public LiveBase
	{
	public:
		LiveWriter();
		virtual ~LiveWriter();

		void ReOpen();
		uint16_t OpenAudioStream(const AudioHeader &header);
		uint16_t OpenVideoStream(const VideoHeader &header);
		inline uint16_t OpenChatStream()			{return OpenStream(LiveStreamType::Chat);}
		inline uint16_t OpenSubtitleStream()		{return OpenStream(LiveStreamType::Subtitle);}
		inline uint16_t OpenCommandStream()		{return OpenStream(LiveStreamType::Command);}
		inline uint16_t OpenCustomStream(const std::string &header=std::string())
		{return OpenStream(LiveStreamType::Custom,header,true);}

		bool CloseStream(const uint16_t &stream_id);
		bool WritePacket(const uint16_t &stream_id,const Packet& pkt);
		void Flush();

		LiveChunkEvent			OnChunk;
	private:
		CoreObjectLib::CoreObject *_core;
		size_t _pos;
		std::string _chunk;
		uint16_t _last_stream_id;
		uint32_t _last_chunk_id;

		void WriteBytes(const std::string &bytes);
		uint16_t OpenStream(const LiveStreamType &type,const std::string& header=std::string(),bool create_header = false);
	};
}

#endif