//
//  LiveReader.cpp
//  Decoder
//
//  Created by Oleg on 07.02.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#include "LiveReader.h"

using namespace LiveFormatLib;
using namespace CoreObjectLib;
using namespace std;

LiveReader::LiveReader()
	:LiveBase()
{
	ReOpen();
}

LiveReader::~LiveReader()
{
    
}

void LiveReader::ReOpen()
{
	_last_chunk_id = 0;
	_next_is_part = false;
	_opened_streams.clear();
	_state = LiveReaderState::Idle;
}

bool LiveReader::Read(const std::string &chunk)
{
	RETURN_MSG_IF_TRUE(chunk.size()<4,"Invalid chunk size: "<<chunk.size());

	if (chunk.size() == kLiveChunkSize)
		return ParseChunk(chunk);
	else
		return ParseStreams(chunk);
}

bool LiveReader::ParseChunk(const std::string &chunk)
{
	uint32_t chunk_id;
	uint32_t size_to_parse;
	LivePacketType packet_type;
	
	_pos = (uint8_t*)chunk.c_str();
	_end = _pos + chunk.size();
	chunk_id = Utils::GetBe32(_pos); _pos +=4;
	RETURN_MSG_IF_TRUE(chunk_id != _last_chunk_id+1 && _last_chunk_id != 0, 
		"Parsed invalid chunk id: "<<chunk_id<<", last chunk id: "<<_last_chunk_id);
	_last_chunk_id = chunk_id;

	while (_end-_pos>0)
	{
		switch (_state)
		{
		case LiveReaderState::Idle:
			if (_end-_pos < 4)
			{
				_parsed_pkt_size = _end-_pos;
				memcpy(_packed_pkt_size,_pos,_parsed_pkt_size);
				_state = LiveReaderState::Size;
				return true;
			}
			_part.clear();
			_pkt_size = Utils::GetBe32(_pos); _pos +=4;
			RETURN_IF_FALSE(ParsePacketToEnd());
			if (_pkt_size != 0)
				_state = LiveReaderState::Part;
		break;
		case LiveReaderState::Part:
			size_to_parse = Utils::GetBe32(_pos); _pos+=5;
			RETURN_IF_FALSE(ParsePacketToEnd());
			if (_pkt_size == 0)
				_state = LiveReaderState::Idle;
		break;
		case LiveReaderState::Size:
			_pos+=4;
			packet_type = (LivePacketType)*_pos;
			RETURN_MSG_IF_FALSE(packet_type==LivePacketType::Part,"Invalid packet type("<<(int)packet_type<<") while parsing chunk's size");
			_pos++;
			memcpy(_packed_pkt_size+_parsed_pkt_size,_pos,4-_parsed_pkt_size);
			_pos += 4-_parsed_pkt_size;
			_pkt_size = Utils::GetBe32(_packed_pkt_size);
			RETURN_IF_FALSE(ParsePacketToEnd());
			if (_pkt_size == 0)
				_state = LiveReaderState::Idle;
			else
				_state = LiveReaderState::Part;
		break;
		}
	}
	return true;
}

bool LiveReader::ParsePacketToEnd()
{
	uint32_t size_to_parse;
	uint8_t *pos,*end;
	LivePacketType packet_type;
	
	size_to_parse = min<uint32_t>(_pkt_size,_end-_pos);
	_part.append(string((char*)_pos,size_to_parse));
	_pos += size_to_parse;
	_pkt_size -= size_to_parse;

	if (_pkt_size == 0)
	{
		RETURN_MSG_IF_TRUE(_part.size() < 3,"Invalid packet size("<<_part.size()<<")");
		pos = (uint8_t*)_part.c_str(); 
		end = pos + _part.size();
		packet_type = (LivePacketType)*pos; ++pos;
		switch(packet_type)
		{
		case LivePacketType::Flush:
		case LivePacketType::Part:
			return true;
		break;
		case LivePacketType::OpenStream:
			return ParseOpenStream(pos,end);
		break;
		case LivePacketType::CloseStream:
			return ParseCloseStream(pos,end);
		break;
		case LivePacketType::Packet:
			return ParsePacket(pos,end);
		break;
		default:
			COErr::Set() << "Invalid live packet type: "<<(int)packet_type;
			return false;
		break;
		}
	}
	return true;
}

bool LiveReader::ParseOpenStream(uint8_t *pos, uint8_t *end)
{
	LiveStreamType type;
	uint16_t stream_id;
	AudioHeader audio_header;
	VideoHeader video_header;
	uint16_t extra_data_size;
	string packed_part;
	uint8_t *packed_ptr;

	type = (LiveStreamType)*pos; ++pos;
	stream_id = Utils::GetBe16(pos); pos +=2;
	RETURN_MSG_IF_TRUE(_opened_streams.count(stream_id)==1,"Stream id "<<stream_id<<" is already opened");
	switch (type)
	{
	case LiveStreamType::Audio:
		RETURN_MSG_IF_TRUE(end-pos<20,"Invalid parse size("<<end-pos<<") while parsing audio stream");

		audio_header.codec = (LiveCodec)Utils::GetByte(pos); ++pos;
		audio_header.sample_rate = Utils::GetBe32(pos); pos+=4;
		audio_header.channels = Utils::GetByte(pos); pos++;
		audio_header.bit_rate = Utils::GetBe32(pos); pos+=4;
		audio_header.max_bit_rate = Utils::GetBe32(pos); pos+=4;
		audio_header.bitstream_size = Utils::GetBe32(pos); pos+=4;
		extra_data_size = Utils::GetBe16(pos); pos+=2;

		RETURN_MSG_IF_TRUE(end-pos != extra_data_size,
			"Invalid extradata size("<<extra_data_size<<"), remaining size is: "<<end-pos<<" while parsing audio header");
		audio_header.extra_data.assign((char*)pos,extra_data_size);
		OnOpenAudioStream(stream_id,audio_header);
	break;
	case LiveStreamType::Video:
		RETURN_MSG_IF_TRUE(end-pos<11,"Invalid parse size("<<end-pos<<") while parsing video stream");
		video_header.codec = (LiveCodec)Utils::GetByte(pos); ++pos;
		video_header.width = Utils::GetBe16(pos); pos+=2;
		video_header.height = Utils::GetBe16(pos); pos+=2;
		video_header.bit_rate = Utils::GetBe32(pos); pos+=4;
		extra_data_size = Utils::GetBe16(pos); pos+=2;
		
		RETURN_MSG_IF_TRUE(end-pos != extra_data_size,
			"Invalid extradata size("<<extra_data_size<<"), remaining size is: "<<end-pos<<" while parsing video header");
		video_header.extra_data.assign((char*)pos,extra_data_size);
		OnOpenVideoStream(stream_id,video_header);
	break;
	case LiveStreamType::Chat:
		OnOpenChatStream(stream_id);
	break;
	case LiveStreamType::Command:
		OnOpenCommandStream(stream_id);
	break;
	case LiveStreamType::Subtitle:
		OnOpenSubtitleStream(stream_id);
	break;
	case LiveStreamType::Custom:
		extra_data_size = Utils::GetBe16(pos); pos+=2;
		RETURN_MSG_IF_TRUE(end-pos != extra_data_size,
			"Invalid extradata size("<<extra_data_size<<"), remaining size is: "<<end-pos<<" while parsing custom header");
		audio_header.extra_data.assign((char*)pos,extra_data_size);
		OnOpenCustomStream(stream_id,audio_header.extra_data);
	break;
	default:
		COErr::Set() << "Invalid stream type("<<(int)type<<" while parsing open stream";
		return false;
	break;
	}


	packed_part.resize(_part.size() + 4);
	packed_ptr = (uint8_t *)packed_part.c_str();
	Utils::PutBe32(packed_ptr,_part.size()); packed_ptr+=4;
	memcpy(packed_ptr,_part.c_str(),_part.size());

	_opened_streams.insert(make_pair(stream_id,packed_part));
	return true;
}

bool LiveReader::ParseCloseStream(uint8_t *pos, uint8_t *end)
{
	uint16_t stream_id;

	RETURN_MSG_IF_FALSE(end-pos==2,"Invalid close stream chunk size: "<<end-pos);
	stream_id = Utils::GetBe16(pos);
	RETURN_MSG_IF_TRUE(_opened_streams.count(stream_id)==0,"Trying to close unexisting stream("<<stream_id<<")");
	_opened_streams.erase(stream_id);
	OnCloseStream(stream_id);
	return true;
}

bool LiveReader::ParsePacket(uint8_t *pos, uint8_t *end)
{
	uint16_t stream_id;
	Packet packet;

	RETURN_MSG_IF_TRUE (end-pos < 6,"Invalid size("<<end-pos<<") while parsing packet");
	stream_id = Utils::GetBe16(pos); pos+=2;
	RETURN_MSG_IF_TRUE(_opened_streams.count(stream_id)==0,"Trying to parse packet of unexisting stream("<<stream_id<<")");
	packet.pts = Utils::GetBe32(pos); pos+=4;
	packet.data.assign((char*)pos,end-pos);
	OnPacket(stream_id,packet);
	return true;
}

bool LiveReader::ParseStreams(const string &chunk)
{
	uint8_t *pos,*end;
	uint32_t part_size;
	LivePacketType type;

	pos = (uint8_t*)chunk.c_str();
	end = pos + chunk.size();

	while (end-pos > 5)
	{
		part_size = Utils::GetBe32(pos); pos +=4;
		_part.assign((char*)pos,part_size);
		type = (LivePacketType)*pos;
		RETURN_MSG_IF_FALSE(type == LivePacketType::OpenStream,"Invalid packet type("<<(int)type<<") while parsing streams");
		pos++;--part_size;
		RETURN_MSG_IF_TRUE(part_size > end-pos,"Invalid stream part size("<<part_size<<") while parsing streams");
		RETURN_IF_FALSE(ParseOpenStream(pos,pos+part_size));
		pos += part_size;
	}

	RETURN_MSG_IF_TRUE(end-pos !=0,"Invalid end-pos value("<<end-pos<<") while parsing streams. Chunk size is: "<<chunk.size());
	return true;
}