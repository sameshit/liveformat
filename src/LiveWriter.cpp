#include "LiveWriter.h"

using namespace LiveFormatLib;
using namespace std;
using namespace CoreObjectLib;

LiveWriter::LiveWriter()
	:LiveBase()
{
	_chunk.resize(kLiveChunkSize);
	ReOpen();
}

LiveWriter::~LiveWriter()
{
}

void LiveWriter::ReOpen()
{
	_pos = 0;
	_last_stream_id = 0;
	_last_chunk_id = 0;
	_opened_streams.clear();
}

uint16_t LiveWriter::OpenAudioStream(const AudioHeader &audio_header)
{
	size_t header_size;
	uint8_t *pos;
	uint16_t stream_id;
	LiveStreamType type;
	string packed_info;

	header_size = (audio_header.extra_data.size() + 20+3+1+4);

	stream_id = ++_last_stream_id;
	type = LiveStreamType::Audio;

	packed_info.resize(header_size);
	pos = (uint8_t*)packed_info.c_str();
	Utils::PutBe32(pos,header_size-4);					pos+=4;
	Utils::PutByte(pos,(int)LivePacketType::OpenStream);++pos;
	Utils::PutByte(pos,(int)type);					++pos;
	Utils::PutBe16(pos,stream_id);					pos+=2;
	Utils::PutByte(pos,(int)audio_header.codec);		++pos;
	Utils::PutBe32(pos,audio_header.sample_rate);		pos+=4;
	Utils::PutByte(pos,audio_header.channels);			++pos;
	Utils::PutBe32(pos,audio_header.bit_rate);			pos+=4;
	Utils::PutBe32(pos,audio_header.max_bit_rate);		pos+=4;
	Utils::PutBe32(pos,audio_header.bitstream_size);	pos+=4;
	Utils::PutBe16(pos,audio_header.extra_data.size()); pos+=2;

	memcpy(pos,audio_header.extra_data.c_str(),audio_header.extra_data.size());
	pos += audio_header.extra_data.size();
	
	_opened_streams.insert(make_pair(stream_id,packed_info));
	WriteBytes(packed_info);
	return stream_id;
}

uint16_t LiveWriter::OpenVideoStream(const VideoHeader &video_header)
{
	size_t header_size;
	uint8_t *pos;
	uint16_t stream_id;
	LiveStreamType type;
	string packed_info;

	stream_id = ++_last_stream_id;
	type = LiveStreamType::Video;


	header_size = (video_header.extra_data.size() + 11+3+1+4);
	packed_info.resize(header_size);
	pos = (uint8_t*)packed_info.c_str();
	
	Utils::PutBe32(pos,header_size-4);					pos+=4;
	Utils::PutByte(pos,(int)LivePacketType::OpenStream);++pos;
	Utils::PutByte(pos,(int)type);						++pos;
	Utils::PutBe16(pos,stream_id);						pos+=2;
	Utils::PutByte(pos,(int)video_header.codec);		++pos;
	Utils::PutBe16(pos,video_header.width);				pos+=2;
	Utils::PutBe16(pos,video_header.height);			pos+=2;
	Utils::PutBe32(pos,video_header.bit_rate);			pos+=4;
	Utils::PutBe16(pos,video_header.extra_data.size()); pos+=2;
	
	memcpy(pos,video_header.extra_data.c_str(),video_header.extra_data.size());
	pos += video_header.extra_data.size();
	
	_opened_streams.insert(make_pair(_last_stream_id,packed_info));
	WriteBytes(packed_info);
	return stream_id;
}

uint16_t LiveWriter::OpenStream(const LiveStreamType &stream_type,const string &header,bool create_header)
{
	size_t header_size;
	uint8_t *pos;
	uint16_t stream_id;
	LiveStreamType type;
	string packed_info;

	if (create_header)
		header_size = 10 + header.size();
	else
		header_size = 8;
	packed_info.resize(header_size);
	stream_id = ++_last_stream_id;
	type = stream_type;
	
	pos = (uint8_t*)packed_info.c_str();			
	Utils::PutBe32(pos,header_size-4);					pos+=4;
	Utils::PutByte(pos,(int)LivePacketType::OpenStream);++pos;
	Utils::PutByte(pos,(int)type);						++pos;
	Utils::PutBe16(pos,stream_id);						pos+=2;
	if (create_header)
	{
		Utils::PutBe16(pos,header.size()); pos +=2;
		memcpy(pos,header.c_str(),header.size());
	}

	_opened_streams.insert(make_pair(stream_id,packed_info));
	WriteBytes(packed_info);
	return stream_id;
}

bool LiveWriter::CloseStream(const uint16_t &stream_id)
{
	size_t header_size;
	uint8_t *pos;
	string packed_info;

	RETURN_MSG_IF_TRUE(_opened_streams.count(stream_id)==0,"Invalid stream id("<<stream_id<<")");

	_opened_streams.erase(stream_id);
	
	header_size = 7;
	packed_info.resize(header_size);
	pos = (uint8_t*)packed_info.c_str();
	Utils::PutBe32(pos,header_size-4);					pos+=4;
	Utils::PutByte(pos,(int)LivePacketType::CloseStream);++pos;
	Utils::PutBe16(pos,stream_id);				 pos+=2;

	WriteBytes(packed_info);
	return true;
}

bool LiveWriter::WritePacket(const uint16_t &stream_id, const Packet &pkt)
{
	string pkt_data;
	size_t size;
	uint8_t *pos;

	RETURN_MSG_IF_TRUE(_opened_streams.count(stream_id)==0,"Invalid stream id("<<stream_id<<")");
	
	size = 11+pkt.data.size();
	pkt_data.resize(size);
	pos = (uint8_t*)pkt_data.c_str();
	Utils::PutBe32(pos,size-4);		pos+=4;
	Utils::PutByte(pos,(int)LivePacketType::Packet);++pos;
	Utils::PutBe16(pos,stream_id);				pos+=2;
	Utils::PutBe32(pos,pkt.pts);					pos+=4;
	memcpy(pos,pkt.data.c_str(),pkt.data.size()); pos += pkt.data.size();

	WriteBytes(pkt_data);
	return true;
}

void LiveWriter::WriteBytes(const std::string &bytes)
{
	size_t bytes_pos,rem_bytes,rem_chunk,copy_size;
	uint8_t *chunk_ptr,*bytes_ptr;

	for (bytes_pos = 0;
		bytes_pos < bytes.size();
		)
	{
		rem_bytes = bytes.size() - bytes_pos;
		if (_pos == 0)
		{
			chunk_ptr = ((uint8_t*)_chunk.c_str());
			Utils::PutBe32(chunk_ptr,++_last_chunk_id); chunk_ptr+=4; _pos+=4;
			if (bytes_pos != 0)
			{
				Utils::PutBe32(chunk_ptr,min<size_t>(rem_bytes+1,kLiveChunkSize-10));	chunk_ptr +=4;_pos+=4;	
				Utils::PutByte(chunk_ptr,(int)LivePacketType::Part);				chunk_ptr +=1;_pos+=1;
			}
		}

		rem_chunk = kLiveChunkSize - _pos;
		copy_size = min<size_t>(rem_bytes,rem_chunk);
		chunk_ptr = ((uint8_t*)_chunk.c_str())+_pos;
		bytes_ptr = ((uint8_t*)bytes.c_str())+bytes_pos;
		
		memcpy(chunk_ptr,bytes_ptr,copy_size);
		_pos+=copy_size;
		bytes_pos+=copy_size;

		assert(_pos <= kLiveChunkSize);
		if (_pos == kLiveChunkSize)
		{
			OnChunk(_chunk);
			_pos = 0;
		}
	}
}

void LiveWriter::Flush()
{
	uint8_t *chunk_ptr,*end;

	if (_pos == 0)
		return;

	chunk_ptr = (uint8_t*)(_chunk.c_str()) + _pos;
	end		  = (uint8_t*)(_chunk.c_str()) + _chunk.size();
	Utils::PutBe32(chunk_ptr,end-chunk_ptr-4); chunk_ptr+=4;
	Utils::PutByte(chunk_ptr,(int)LivePacketType::Flush);
	_pos = 0;
	OnChunk(_chunk);
}