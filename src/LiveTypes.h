
//
//  Header.h
//  MPeer
//
//  Created by Oleg on 15.02.13.
//
//

#ifndef MLIVETYPESPeer_Header_h
#define MLIVETYPESPeer_Header_h

#include "../../FrameUtils/src/FrameUtils.h"

namespace LiveFormatLib
{

	enum class LiveCodec
	{
		None	= 0xFF,
		H264	= 0,
		AAC		= 1,
		VP8		= 2,
		VP9		= 3,
		Opus	= 4,
		Vorbis	= 5,
		JPEG	= 6,
	};

	class Frame
	{
	public:
		uint8_t*    data;
		ssize_t     size;
		uint32_t    pts;
	};

	class VideoHeader
	{
	public:
		LiveCodec   codec;
		uint16_t    width;
		uint16_t    height;
		uint32_t    bit_rate;
		std::string extra_data;
	};

	class AudioHeader
	{
	public:
		LiveCodec   codec;
		uint32_t    sample_rate;
		uint8_t     channels;
		uint32_t    bit_rate;
		uint32_t    max_bit_rate;
		uint32_t    bitstream_size;
		std::string extra_data;
	};

	class Packet
	{
	public:
		std::string     data;
		uint32_t        pts;
	};

	const size_t kLiveChunkSize = 1300;

	enum class LiveStreamType
	{
		Audio = 1,
		Video,
		Subtitle,
		Command,
		Chat,
		Custom
	};

	enum class LivePacketType
	{
		OpenStream = 1,
		Packet,
		Part,
		CloseStream,
		Flush,
	};

	typedef CoreObjectLib::Event<const std::string&>					LiveChunkEvent; 
	typedef CoreObjectLib::Event<const uint16_t &,const AudioHeader &>  LiveAudioStreamEvent;
	typedef CoreObjectLib::Event<const uint16_t &,const VideoHeader &>  LiveVideoStreamEvent;
	typedef CoreObjectLib::Event<const uint16_t &>						LiveChatStreamEvent;
	typedef CoreObjectLib::Event<const uint16_t &>						LiveCommandStreamEvent;
	typedef CoreObjectLib::Event<const uint16_t &>						LiveSubtitleStreamEvent;
	typedef CoreObjectLib::Event<const uint16_t &,const std::string&>	LiveCustomStreamEvent;
	typedef CoreObjectLib::Event<const uint16_t &>						LiveCloseStreamEvent;
	typedef CoreObjectLib::Event<const uint16_t&,const Packet &>		LivePacketEvent;
	typedef CoreObjectLib::Event<>										ResetEvent;
}
#endif
