#include "LiveBase.h"

using namespace LiveFormatLib;
using namespace std;
using namespace CoreObjectLib;

LiveBase::LiveBase()
{

}

LiveBase::~LiveBase()
{

}

string LiveBase::SerializeStreams()
{
	size_t total_size;
	string serialize;
	uint8_t *pos;

	total_size = 0;
	for (auto it = _opened_streams.begin(); 
		 it != _opened_streams.end();
		 ++it)
		total_size += (*it).second.size();

	serialize.resize(total_size);
	pos = (uint8_t*)serialize.c_str();
	
	for (auto it = _opened_streams.begin(); 
		 it != _opened_streams.end();
		 ++it)
	{
		memcpy(pos,(*it).second.c_str(),(*it).second.size());
		pos += (*it).second.size();
	}
	
	return serialize;
}