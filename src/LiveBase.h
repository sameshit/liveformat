#ifndef LIVEBASEE__H
#define LIVEBASEE__H

#include "LiveTypes.h"

namespace LiveFormatLib
{
	class LiveBase
	{
	public:
		LiveBase();
		virtual ~LiveBase();

		std::string SerializeStreams();
	protected:
		std::unordered_map<uint16_t,std::string> _opened_streams;
	};
}

#endif